﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace integrationones.Models
{
    class Server
    {
        public Server(string name, string address, string port, string login, string password)
        {
            this.ServerName = name ?? throw new ArgumentNullException(nameof(name));
            this.ServerAddress = address ?? throw new ArgumentNullException(nameof(address));
            this.ServerPort = port;
            this.ServerLogin = login ?? throw new ArgumentNullException(nameof(login));
            this.ServerPassword = password ?? throw new ArgumentNullException(nameof(password));
        }

        public string ServerName { get; set; }
        public string ServerAddress { get; set; }
        public string ServerPort { get; set; }
        public string ServerLogin { get; set; }
        public string ServerPassword { get; set; }

        public override string ToString()
        {
            return ServerAddress + " - " + ServerName;
        }
    }
}
