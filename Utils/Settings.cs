﻿using integrationones.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace integrationones.Utils
{
    static class Settings
    {
        public static List<Server> ServersList = new List<Server> { };

        public static void UploadServers()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(ConfigurationManager.AppSettings.Get("serversFileName"));

            foreach (XmlNode node in doc.DocumentElement)
            {
                string name = node.Attributes[0].Value;
                string address = node["Address"].InnerText;
                string port = node["Port"].InnerText;
                string login = node["Login"].InnerText;
                string password = node["Password"].InnerText;
                ServersList.Add(new Server(name, address, port, login, password));
            }
        }

        public static Server GetServerByComboSelected(string[] words)
        {
            foreach (Server el in ServersList)
            {
                if (el.ServerAddress == words[0] && el.ServerName == words[2])
                    return el;
            }
            return null;
        }
    }
}
