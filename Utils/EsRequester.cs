﻿using integrationones.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace integrationones.Utils
{
    static class ESRequester
    {
        public static List<ESEntity> GetCatalogsList(Server server)
        {
            List<ESEntity> catalogsList = new List<ESEntity>() { };
            string url = "http://" + server.ServerAddress + "/" + server.ServerName + "/odata/standard.odata";
            // Create a WebRequest with the specified URL. 
            WebRequest myWebRequest = WebRequest.Create(url);
            myWebRequest.Credentials = new NetworkCredential(server.ServerLogin, server.ServerPassword);
            WebResponse myWebResponse = myWebRequest.GetResponse();
            using (Stream dataStream = myWebResponse.GetResponseStream())
            {
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(responseFromServer);
                XmlElement xRoot = doc.DocumentElement;

                foreach (XmlNode xnode in xRoot)
                {
                    foreach (XmlNode childnode in xnode.ChildNodes)
                    {
                        if (childnode.Name == "collection")
                        {
                            var collectionName = childnode.Attributes[0].Value;
                            var collectionUrl = childnode["atom:title"].InnerText;
                            catalogsList.Add(new ESEntity(
                                collectionUrl,
                                collectionName
                            ));
                            // Console.WriteLine("href: {0}", );
                            // Console.WriteLine("name: {0}", );
                        }
                        
                        // если узел - company

                    }
                }
                // Display the content.  
                // Console.WriteLine(responseFromServer);

                
            }
            myWebResponse.Close();
            foreach (ESEntity el in catalogsList)
            {
                Console.WriteLine(el.name);
            }
            return catalogsList;
        }
    }
}
