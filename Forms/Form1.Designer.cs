﻿namespace integrationones
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.uploadPage = new System.Windows.Forms.TabPage();
            this.uploadStatusRich = new System.Windows.Forms.RichTextBox();
            this.btUploadData = new System.Windows.Forms.Button();
            this.choseServerToUploadCb = new System.Windows.Forms.ComboBox();
            this.dataPage = new System.Windows.Forms.TabPage();
            this.settingsPage = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.listServerPage = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ServerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServerAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServerLogin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ServerPassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addServerPage = new System.Windows.Forms.TabPage();
            this.btAddServer = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.uploadPage.SuspendLayout();
            this.settingsPage.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.listServerPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.addServerPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.uploadPage);
            this.tabControl1.Controls.Add(this.dataPage);
            this.tabControl1.Controls.Add(this.settingsPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(800, 450);
            this.tabControl1.TabIndex = 3;
            // 
            // uploadPage
            // 
            this.uploadPage.Controls.Add(this.uploadStatusRich);
            this.uploadPage.Controls.Add(this.btUploadData);
            this.uploadPage.Controls.Add(this.choseServerToUploadCb);
            this.uploadPage.Location = new System.Drawing.Point(4, 22);
            this.uploadPage.Name = "uploadPage";
            this.uploadPage.Size = new System.Drawing.Size(792, 424);
            this.uploadPage.TabIndex = 2;
            this.uploadPage.Text = "Загрузка данных";
            this.uploadPage.UseVisualStyleBackColor = true;
            // 
            // uploadStatusRich
            // 
            this.uploadStatusRich.Location = new System.Drawing.Point(8, 55);
            this.uploadStatusRich.Name = "uploadStatusRich";
            this.uploadStatusRich.Size = new System.Drawing.Size(776, 305);
            this.uploadStatusRich.TabIndex = 2;
            this.uploadStatusRich.Text = "";
            // 
            // btUploadData
            // 
            this.btUploadData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.btUploadData.Location = new System.Drawing.Point(248, 366);
            this.btUploadData.Name = "btUploadData";
            this.btUploadData.Size = new System.Drawing.Size(331, 50);
            this.btUploadData.TabIndex = 1;
            this.btUploadData.Text = "Загрузить данные";
            this.btUploadData.UseVisualStyleBackColor = true;
            this.btUploadData.Click += new System.EventHandler(this.BtUploadData_Click);
            // 
            // choseServerToUploadCb
            // 
            this.choseServerToUploadCb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.choseServerToUploadCb.FormattingEnabled = true;
            this.choseServerToUploadCb.Location = new System.Drawing.Point(8, 21);
            this.choseServerToUploadCb.Name = "choseServerToUploadCb";
            this.choseServerToUploadCb.Size = new System.Drawing.Size(776, 28);
            this.choseServerToUploadCb.TabIndex = 0;
            // 
            // dataPage
            // 
            this.dataPage.Location = new System.Drawing.Point(4, 22);
            this.dataPage.Name = "dataPage";
            this.dataPage.Padding = new System.Windows.Forms.Padding(3);
            this.dataPage.Size = new System.Drawing.Size(792, 424);
            this.dataPage.TabIndex = 0;
            this.dataPage.Text = "Данные";
            this.dataPage.UseVisualStyleBackColor = true;
            // 
            // settingsPage
            // 
            this.settingsPage.Controls.Add(this.tabControl2);
            this.settingsPage.Location = new System.Drawing.Point(4, 22);
            this.settingsPage.Name = "settingsPage";
            this.settingsPage.Padding = new System.Windows.Forms.Padding(3);
            this.settingsPage.Size = new System.Drawing.Size(792, 424);
            this.settingsPage.TabIndex = 1;
            this.settingsPage.Text = "Настройки";
            this.settingsPage.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.listServerPage);
            this.tabControl2.Controls.Add(this.addServerPage);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(786, 418);
            this.tabControl2.TabIndex = 0;
            // 
            // listServerPage
            // 
            this.listServerPage.Controls.Add(this.dataGridView1);
            this.listServerPage.Location = new System.Drawing.Point(4, 22);
            this.listServerPage.Name = "listServerPage";
            this.listServerPage.Padding = new System.Windows.Forms.Padding(3);
            this.listServerPage.Size = new System.Drawing.Size(778, 392);
            this.listServerPage.TabIndex = 0;
            this.listServerPage.Text = "Сервера";
            this.listServerPage.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ServerName,
            this.ServerAddress,
            this.ServerLogin,
            this.ServerPassword});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.SystemColors.Window;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(772, 386);
            this.dataGridView1.TabIndex = 0;
            // 
            // ServerName
            // 
            this.ServerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ServerName.HeaderText = "Название сервера";
            this.ServerName.Name = "ServerName";
            // 
            // ServerAddress
            // 
            this.ServerAddress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ServerAddress.HeaderText = "Адрес сервера";
            this.ServerAddress.Name = "ServerAddress";
            // 
            // ServerLogin
            // 
            this.ServerLogin.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ServerLogin.HeaderText = "Логин";
            this.ServerLogin.Name = "ServerLogin";
            // 
            // ServerPassword
            // 
            this.ServerPassword.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ServerPassword.HeaderText = "Пароль";
            this.ServerPassword.Name = "ServerPassword";
            // 
            // addServerPage
            // 
            this.addServerPage.Controls.Add(this.btAddServer);
            this.addServerPage.Controls.Add(this.textBox4);
            this.addServerPage.Controls.Add(this.textBox3);
            this.addServerPage.Controls.Add(this.textBox2);
            this.addServerPage.Controls.Add(this.textBox1);
            this.addServerPage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.addServerPage.Location = new System.Drawing.Point(4, 22);
            this.addServerPage.Name = "addServerPage";
            this.addServerPage.Padding = new System.Windows.Forms.Padding(3);
            this.addServerPage.Size = new System.Drawing.Size(778, 392);
            this.addServerPage.TabIndex = 1;
            this.addServerPage.Text = "Добавить сервер";
            this.addServerPage.UseVisualStyleBackColor = true;
            // 
            // btAddServer
            // 
            this.btAddServer.Location = new System.Drawing.Point(244, 342);
            this.btAddServer.Name = "btAddServer";
            this.btAddServer.Size = new System.Drawing.Size(299, 44);
            this.btAddServer.TabIndex = 4;
            this.btAddServer.Text = "Добавить сервер";
            this.btAddServer.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox4.Location = new System.Drawing.Point(669, 6);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(103, 26);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox3.Location = new System.Drawing.Point(404, 38);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(368, 26);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextChanged += new System.EventHandler(this.TextBox3_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox2.Location = new System.Drawing.Point(6, 38);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(378, 26);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.textBox1.Location = new System.Drawing.Point(6, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(657, 26);
            this.textBox1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Интегратор 1с";
            this.tabControl1.ResumeLayout(false);
            this.uploadPage.ResumeLayout(false);
            this.settingsPage.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.listServerPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.addServerPage.ResumeLayout(false);
            this.addServerPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage uploadPage;
        private System.Windows.Forms.RichTextBox uploadStatusRich;
        private System.Windows.Forms.Button btUploadData;
        private System.Windows.Forms.ComboBox choseServerToUploadCb;
        private System.Windows.Forms.TabPage dataPage;
        private System.Windows.Forms.TabPage settingsPage;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage listServerPage;
        private System.Windows.Forms.TabPage addServerPage;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btAddServer;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServerAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServerLogin;
        private System.Windows.Forms.DataGridViewTextBoxColumn ServerPassword;
    }
}

