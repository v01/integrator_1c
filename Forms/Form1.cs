﻿using integrationones.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using integrationones.Utils;

namespace integrationones
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Settings.UploadServers();
            SetChoseServerToUploadCb();
            
        }

        private void SetChoseServerToUploadCb()
        {
            foreach (var elem in Settings.ServersList)
            {
                dataGridView1.Rows.Add(elem);
                choseServerToUploadCb.Items.Add(elem.ToString());
            }

            
        }

        private void TextBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void BtUploadData_Click(object sender, EventArgs e)
        {
            Server selectedServer = Settings.GetServerByComboSelected(
                choseServerToUploadCb.SelectedItem.ToString().Split(new char[] { ' ' })
            );
            // uploadStatusRich.Text = 
            List<ESEntity> lst = ESRequester.GetCatalogsList(selectedServer);
            foreach (ESEntity el in lst)
            {
                Console.WriteLine(el.name);
            }
        }
    }
}
